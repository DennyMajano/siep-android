package com.siep.movil.register.main;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.siep.movil.register.fragment.PhoneCodeVerification;
import com.siep.movil.register.fragment.PhoneVerification;
import com.siep.movil.register.fragment.RegisterUserFirst;
import com.siep.movil.register.fragment.RegisterUserSecond;
import com.siep.movil.register.fragment.RegisterUserThird;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0: return new RegisterUserFirst();
            case 1: return new RegisterUserSecond();
            case 2: return new PhoneVerification();
            case 3: return new PhoneCodeVerification();
            default: return new RegisterUserFirst();
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    @Override
    public int getCount() {
        // Show 5 total pages.
        return 4;
    }
}