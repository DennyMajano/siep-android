package com.siep.movil.register.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.PhoneAuthProvider;
import com.siep.movil.R;
import com.siep.movil.register.RegisterActivity;
import com.siep.movil.register.main.CViewPager;

import java.util.concurrent.TimeUnit;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhoneVerification extends Fragment {
    Button next;
    EditText phone;

    RegisterActivity refActivity;
    public PhoneVerification() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        refActivity = (RegisterActivity)getActivity();
        View view = inflater.inflate(R.layout.fragment_phone_verification, container, false);
        init(view);
        return view;
    }

    private void init(View view){
        phone = view.findViewById(R.id.etPhoneNumber);
        next = view.findViewById(R.id.btn_send_code);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phoneNumberIsNotEmpty()){
                    refActivity.putInUser("telefono","+503"+phone.getText().toString());
                    refActivity.verifyPhoneExist();


                }
                else {
                    Toast.makeText(getContext(),"Proporcione un numero de teléfono válido",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });
    }



    private boolean phoneNumberIsNotEmpty(){
        return phone.getText().toString().trim().length()==8;
    }


}
