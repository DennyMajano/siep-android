package com.siep.movil.register.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.siep.movil.R;
import com.siep.movil.register.RegisterActivity;
import com.siep.movil.register.main.CViewPager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterUserSecond extends Fragment {
    RegisterActivity refActivity;
    int indexMunicipioSelection=0;

    Map departamentosMunicipios = new HashMap();
    List<String> departamentos;
    List<String> municipios;
    List<String> ocupaciones;

    ArrayAdapter<String> departamentosAdapter;
    ArrayAdapter<String> municipiosAdapter;
    ArrayAdapter<String> ocupacionesAdapter;


    Button next;
    Spinner departamentosList,municipiosList,ocupacionesList;
    EditText mail;
    public RegisterUserSecond() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        refActivity = ((RegisterActivity)getActivity());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register_user_second, container, false);

        departamentos = new ArrayList<>();
        ocupaciones = new ArrayList<>();
        municipios = new ArrayList<>();




        next = view.findViewById(R.id.btn_register_next2);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fieldsNotEmpty()) {
                    setData();
                    CViewPager viewPager = getActivity().findViewById(R.id.view_pager);
                    viewPager.setCurrentItem(2);
                }
                else {
                    Toast.makeText(getContext(),"Por favor llene todos los campos obligatorios",Toast.LENGTH_SHORT).show();
                }
            }
        });
        mail = view.findViewById(R.id.etMail);
        departamentosList = view.findViewById(R.id.spDepartamento);

        municipiosList = view.findViewById(R.id.spMunicipio);
        ocupacionesList = view.findViewById(R.id.spOcupacion);
        departamentosAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item,departamentos);
        municipiosAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item,municipios);
        ocupacionesAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item,ocupaciones);
        departamentosList.setAdapter(departamentosAdapter);
        municipiosList.setAdapter(municipiosAdapter);
        ocupacionesList.setAdapter(ocupacionesAdapter);

        departamentosList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                municipios.clear();
                Log.d("TAG","asas-----"+position);

                ArrayList<String> a= (ArrayList<String>) departamentosMunicipios.get(departamentos.get(position));
                if(a==null){
                    a=new ArrayList<>();
                }
                Log.d("TAG","asas-----"+a);
                municipios.add("-");
                municipios.addAll(a);
                municipiosAdapter.notifyDataSetChanged();
                municipiosList.setSelection(indexMunicipioSelection);
                indexMunicipioSelection=0;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        if (savedInstanceState==null){
            departamentos.add("-");
            municipios.add("-");
            ocupaciones.add("-");
            DocumentReference docRef = refActivity.getDb().collection("data").document("listas");
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()){
                        DocumentSnapshot document = task.getResult();

                        if (document.exists()) {
                            departamentosMunicipios = (HashMap)document.get("departamentos");

                            ocupaciones.addAll((ArrayList)document.get("ocupaciones"));
                            Log.d("TAG","OCUPACIONES: "+ocupaciones);

                            Set<String> s;
                            s = departamentosMunicipios.keySet();
                            for (String a : s) {
                                departamentos.add(a);
                            }
                            departamentosAdapter.notifyDataSetChanged();
                            ocupacionesAdapter.notifyDataSetChanged();



                        }

                        Log.d("TAG",departamentos+"");
                        Log.d("TAG",departamentosMunicipios+"");

                    }
                }
            });
        }
        else {
            Log.d("TAG","ONSAVEINSTACE RECOVERED");
            departamentosMunicipios = (HashMap) savedInstanceState.getSerializable("departamentosMunicipios");
            departamentos.addAll((List<String>) savedInstanceState.getSerializable("departamentosList"));
            municipios.addAll((List<String>)savedInstanceState.getSerializable("municipiosList"));
            ocupaciones.addAll((List<String>) savedInstanceState.getSerializable("ocupacionesList"));
            departamentosAdapter.notifyDataSetChanged();
            municipiosAdapter.notifyDataSetChanged();
            ocupacionesAdapter.notifyDataSetChanged();
            departamentosList.setSelection(savedInstanceState.getInt("departamento"));
            indexMunicipioSelection = (savedInstanceState.getInt("municipio"));
            ocupacionesList.setSelection(savedInstanceState.getInt("ocupacion"));

            mail.setText(savedInstanceState.getString("mail"));

        }


        return  view;
    }





    private void setData(){

        refActivity.putInUser("departamento",departamentos.get(departamentosList.getSelectedItemPosition()));
        refActivity.putInUser("municipio",municipios.get(municipiosList.getSelectedItemPosition()));
        refActivity.putInUser("ocupacion",ocupaciones.get(ocupacionesList.getSelectedItemPosition()));
        if (mail.getText().toString().trim().length()>0)
            refActivity.putInUser("correo",mail.getText().toString().trim());

    }

    private boolean fieldsNotEmpty(){
        return  departamentosList.getSelectedItemPosition()!=0
                && municipiosList.getSelectedItemPosition()!=0
                && ocupacionesList.getSelectedItemPosition() !=0;
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("mail",mail.getText().toString().trim());
        outState.putInt("departamento",departamentosList.getSelectedItemPosition());
        outState.putInt("municipio",municipiosList.getSelectedItemPosition());
        outState.putInt("ocupacion",ocupacionesList.getSelectedItemPosition());
        outState.putSerializable("departamentosMunicipios", (Serializable) departamentosMunicipios);
        outState.putSerializable("departamentosList", (Serializable) departamentos);
        outState.putSerializable("municipiosList", (Serializable) municipios);
        outState.putSerializable("ocupacionesList", (Serializable) ocupaciones);

    }
}
