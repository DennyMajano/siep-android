package com.siep.movil.register.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.siep.movil.R;
import com.siep.movil.historial.main.HistorialActivity;
import com.siep.movil.register.RegisterActivity;
import com.siep.movil.register.main.CViewPager;

import java.util.concurrent.TimeUnit;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhoneCodeVerification extends Fragment {
    Button verifyCode, changePhone;
    EditText etCode;
    RegisterActivity refActivity;


    public PhoneCodeVerification() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        refActivity = (RegisterActivity)getActivity();
        View view = inflater.inflate(R.layout.fragment_phone_code_verification, container, false);
        init(view);
        return view;
    }


    private void init(final View view){
        etCode = view.findViewById(R.id.etCode);
        verifyCode = view.findViewById(R.id.btn_verify_code);
        verifyCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refActivity.verifyVerificationCode(etCode.getText().toString().trim());
                Snackbar.make(view,"Verificado",Snackbar.LENGTH_SHORT).show();
            }
        });

        changePhone = view.findViewById(R.id.btn_change_phone);
        changePhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CViewPager viewPager = getActivity().findViewById(R.id.view_pager);
                viewPager.setCurrentItem(2);
            }
        });


    }


}
