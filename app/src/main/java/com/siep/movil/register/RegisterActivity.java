package com.siep.movil.register;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.siep.movil.R;
import com.siep.movil.historial.main.HistorialActivity;
import com.siep.movil.register.main.CViewPager;
import com.siep.movil.register.main.SectionsPagerAdapter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class RegisterActivity extends AppCompatActivity {


    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    public Map<String,Object> user;
    TabLayout tabs;
    private String mVerificationId;
    CViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        if(savedInstanceState != null){
            mVerificationId = savedInstanceState.getString("mVerificationId");
            user = (Map<String, Object>) savedInstanceState.getSerializable("user");
            Log.d("TAG","CMD"+user+"MVER"+mVerificationId);
        }
        else user = new HashMap<>();
        init();
    }
    public FirebaseFirestore getDb() {
        return db;
    }

    public FirebaseAuth getmAuth() {
        return mAuth;
    }

    public void putInUser(String key, Object value) {
        user.put(key,value);
    }

    private void init(){
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mAuth.setLanguageCode(Locale.getDefault().getLanguage());



        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        Log.d("TAG","CMDONE"+user+"MVER"+mVerificationId);
    }


    public String getPhoneNumber(){
        return user.get("telefono").toString();

    }


    public void sendPhoneVerification(){
        Log.d("TAG","TELEFONO:"+user);


        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                getPhoneNumber(),
                60,
                TimeUnit.SECONDS,
                this,
                mCallBack
        );
    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
           /* String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                etCode.setText(code);
                //verifying the code


                verifyVerificationCode(code);
            }*/
           signInWithPhoneAuthCredential(phoneAuthCredential);
            Toast.makeText(getApplicationContext(),"VALIDADO OnCompleted",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Log.d("TAG", "onVerificationFailed", e);
            Toast.makeText(getApplicationContext(), "Error al verificar número teléfonico", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            Toast.makeText(getApplicationContext(),"onCodeSent",Toast.LENGTH_SHORT).show();
            mVerificationId = s;
            Log.d("TAG", "onCODE sent"+s);

        }
    };
    public void verifyPhoneExist(){
        Log.d("TAG","VERIFYING PHONE EXISTS: "+user.get("telefono"));
        db.collection("usuariosmovil").whereEqualTo("telefono",user.get("telefono")).get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (queryDocumentSnapshots.getDocuments().size()==1){
                            Toast.makeText(getApplicationContext(),"El numero ya está registrado", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Log.d("TAG","Numero sin registrar");
                            Log.d("TAG","PONIENDO NUMERO");

                            Log.d("TAG","NUMERON="+user);
                            sendPhoneVerification();
                            viewPager.setCurrentItem(3);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                    }
                });

    }

    public void verifyVerificationCode(String code) {
        //creating the credential
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
        Toast.makeText(getApplicationContext(),"Credencial creada",Toast.LENGTH_SHORT).show();
        //signing the user
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        getmAuth().signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            user.put("uid",task.getResult().getUser().getUid());
                            saveUser();


                        } else {

                            //verification unsuccessful.. display an error message

                            String message = "Ha ocurrido un error al iniciar sesión";

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Codigo invalido ingresado";
                            }
                            task.getException().printStackTrace();

                        }
                    }
                });
    }



    private void saveUser(){
        db.collection("usuariosmovil").document (user.get("uid").toString()).set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                    redirectUser();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(),"Fallo al guardad usuario",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void redirectUser(){
        //verification successful we will start the profile activity
        Intent intent = new Intent(getApplicationContext(), HistorialActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }




    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("TAG","SAVE STATE"+user+"MVER"+mVerificationId);
        outState.putSerializable("user", (Serializable) user);
        outState.putString("mVerificationId",mVerificationId);
    }
}