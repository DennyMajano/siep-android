package com.siep.movil.register.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.siep.movil.R;
import com.siep.movil.register.main.CViewPager;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterUserThird extends Fragment {
    Button next;
    public RegisterUserThird() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register_user_third, container, false);
        init(view);
        return view;
    }
    private void init(View view){
        next = view.findViewById(R.id.btn_register_next3);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CViewPager viewPager = getActivity().findViewById(R.id.view_pager);
                viewPager.setCurrentItem(3);
            }
        });
    }
}
