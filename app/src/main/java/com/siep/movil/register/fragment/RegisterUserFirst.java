package com.siep.movil.register.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.siep.movil.R;
import com.siep.movil.register.RegisterActivity;
import com.siep.movil.register.main.CViewPager;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterUserFirst extends Fragment {
    RegisterActivity refActivity;
    Button next;
    EditText nombre, apellido, fechaNacimiento;
    RadioGroup sexo;
    String sexoValue="";
    int sexoId =-1;

    public RegisterUserFirst() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        refActivity = ((RegisterActivity)getActivity());
        View view = inflater.inflate(R.layout.fragment_register_user_first, container, false);
        init(view);
        if (savedInstanceState != null){
            nombre.setText(savedInstanceState.getString("nombre"));
            apellido.setText(savedInstanceState.getString("apellido"));
            nombre.setText(savedInstanceState.getString("fechaNacimiento"));
            sexo.check(savedInstanceState.getInt("sexo"));
        }
        return view;
    }

    private void init(View view){
        nombre = view.findViewById(R.id.nombre);
        apellido = view.findViewById(R.id.apellido);
        fechaNacimiento = view.findViewById(R.id.fechaNacimiento);
        sexo = view.findViewById(R.id.rbSexo);
        sexo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                sexoValue =((RadioButton) group.findViewById(checkedId)).getText().toString();
            }
        });
        next = view.findViewById(R.id.btn_register_next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fieldsNotEmpty()){
                    setData();
                    CViewPager viewPager = getActivity().findViewById(R.id.view_pager);
                    viewPager.setCurrentItem(1);
                }
                else{
                    Toast.makeText(getContext(),"Por favor llene todos los campos", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    private void setData(){
        refActivity.putInUser("nombre",nombre.getText().toString());
        refActivity.putInUser("apellido",apellido.getText().toString());
        refActivity.putInUser("fechaNacimiento",fechaNacimiento.getText().toString());

        refActivity.putInUser("sexo",sexoValue);

        Log.d("TAG","USER = "+refActivity.user);
    }

    private boolean  fieldsNotEmpty(){
        return  apellido.getText().toString().trim().length()>0
                && nombre.getText().toString().trim().length()>0
                && sexoValue.length()>0
                && fechaNacimiento.getText().toString().length()>0;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("nombre",nombre.getText().toString().trim());
        outState.putString("apellido",apellido.getText().toString().trim());
        outState.putString("fechaNacimiento",fechaNacimiento.getText().toString().trim());
        outState.putInt("sexo",sexo.getCheckedRadioButtonId());

    }
}
