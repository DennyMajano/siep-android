package com.siep.movil.login;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.siep.movil.R;
import com.siep.movil.historial.main.HistorialActivity;
import com.siep.movil.login.main.SectionsPagerAdapter;

import java.util.concurrent.TimeUnit;

public class LoginActivityR extends AppCompatActivity {

    public void setPhoneNumber(String phoneNumber) {
        Log.d("TAG","TEL:++"+phoneNumber);
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        Log.d("TAG","TEL:"+phoneNumber);
        return this.phoneNumber;
    }

    String phoneNumber;
    private String mVerificationId;

    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_r);
        mAuth = FirebaseAuth.getInstance();
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

    }




    public void sendPhoneVerification(){

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+503"+phoneNumber,
                60,
                TimeUnit.SECONDS,
                this,
                mCallBack
        );
    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
           /* String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                etCode.setText(code);
                //verifying the code


                verifyVerificationCode(code);
            }*/
            signInWithPhoneAuthCredential(phoneAuthCredential);
            Toast.makeText(getApplicationContext(),"VALIDADO OnCompleted",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Log.d("TAG", "onVerificationFailed", e);
            Toast.makeText(getApplicationContext(), "Error al verificar número teléfonico", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            Toast.makeText(getApplicationContext(),"onCodeSent",Toast.LENGTH_SHORT).show();
            mVerificationId = s;
            Log.d("TAG", "onCODE sent"+s);

        }
    };

    public void verifyVerificationCode(String code) {
        //creating the credential
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
        Toast.makeText(getApplicationContext(),"Credencial creada",Toast.LENGTH_SHORT).show();
        //signing the user
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            Log.d("TAG","DENTRO");
                            redirectUser();


                        } else {
                            task.getException().printStackTrace();
                            Log.d("TAG","NO DENTRO");
                            Toast.makeText(getApplicationContext(),"Error al iniciar sesión. Compruebe el codigo",Toast.LENGTH_LONG);

                        }
                    }
                });
    }

    private void redirectUser(){
        //verification successful we will start the profile activity
        Intent intent = new Intent(getApplicationContext(), HistorialActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }



}