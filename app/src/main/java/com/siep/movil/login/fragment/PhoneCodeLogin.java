package com.siep.movil.login.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.siep.movil.R;
import com.siep.movil.login.LoginActivityR;
import com.siep.movil.register.main.CViewPager;

public class PhoneCodeLogin extends Fragment {
    Button verifyCode, changePhone;

    EditText etCode;
    TextView tvPhoneNumber, tvChanguePhone;
    LoginActivityR refActivity;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        refActivity = (LoginActivityR) getActivity();
        View view = inflater.inflate(R.layout.fragment_phone_code_login, container, false);
        init(view);
        Log.d("TAG","-----"+refActivity.getPhoneNumber());
        //tvPhoneNumber.setText(refActivity.getPhoneNumber()+"asasa");

        return view;
    }

    private void init(final View view){
        etCode = view.findViewById(R.id.etCode);
        tvPhoneNumber = view.findViewById(R.id.tvPhoneNumberView);

        verifyCode = view.findViewById(R.id.btn_verify_code);
        verifyCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etCode.getText().toString().trim().length()<6){
                    refActivity.verifyVerificationCode(etCode.getText().toString().trim());

                }
                else{
                    Snackbar.make(view,"Codigo incorrecto",Snackbar.LENGTH_SHORT).show();
                }



            }
        });

        changePhone = view.findViewById(R.id.btn_change_phone);
        changePhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CViewPager viewPager = getActivity().findViewById(R.id.view_pager);
                viewPager.setCurrentItem(0);
            }
        });


    }

}
