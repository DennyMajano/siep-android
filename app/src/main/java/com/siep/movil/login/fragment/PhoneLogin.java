package com.siep.movil.login.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QuerySnapshot;
import com.siep.movil.R;
import com.siep.movil.login.LoginActivityR;
import com.siep.movil.register.RegisterActivity;
import com.siep.movil.register.main.CViewPager;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhoneLogin extends Fragment {
    Button btnSendCode;
    EditText etPhoneNumber;
    LoginActivityR refActivity;
    TextView newAccount;

    FirebaseFirestore db;
    public PhoneLogin() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        db = FirebaseFirestore.getInstance();

        refActivity = (LoginActivityR) getActivity();
        View view = inflater.inflate(R.layout.fragment_phone_login, container, false);

        init(view);
        return  view;
    }

    private void init(View view){
        etPhoneNumber = view.findViewById(R.id.etPhoneNumber);
        btnSendCode = view.findViewById(R.id.btn_send_code);
        btnSendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phoneNumberIsNotEmpty()){
                    Log.d("TAG","PHONE IS NOT EMPTY");
                   verifyPhoneExist(etPhoneNumber.getText().toString().trim());
                }
                else {
                    Toast.makeText(getContext(),"Proporcione un numero de teléfono válido",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });

        newAccount = view.findViewById(R.id.tvNewAccount);
        newAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), RegisterActivity.class));
            }
        });
    }
    private boolean phoneNumberIsNotEmpty(){
        return etPhoneNumber.getText().toString().trim().length()==8;
    }



    private void verifyPhoneExist(final String phoneNumber){
        Log.d("TAG","VERIFYING PHONE EXISTS: "+phoneNumber);
        db.collection("usuariosmovil").whereEqualTo("telefono","+503"+phoneNumber).get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (queryDocumentSnapshots.getDocuments().size()==1){
                            Toast.makeText(getContext(),"Numer válido", Toast.LENGTH_SHORT).show();
                            Log.d("TAG","PONIENDO NUMERO");
                            refActivity.setPhoneNumber(phoneNumber);

                            refActivity.sendPhoneVerification();
                            CViewPager viewPager = getActivity().findViewById(R.id.view_pager);
                            viewPager.setCurrentItem(1);
                        }
                        else{
                            Toast.makeText(getContext(),"Numer no registrado. Por favor registrate", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                    }
                });

    }

}
