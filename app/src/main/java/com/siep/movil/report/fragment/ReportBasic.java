package com.siep.movil.report.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.siep.movil.R;
import com.siep.movil.register.main.CViewPager;
import com.siep.movil.report.ReportActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportBasic extends Fragment {

    Map areasTipo;
    List<String> areas2;
    List<String> tipos;
    ArrayAdapter<String> areaAdapter;
    ArrayAdapter<String> tipoAdapter;

    Spinner tipo, area;
    Button next;
    EditText titulo,descripcion;


    ReportActivity refActivity;
    private int indexTipoSelection=0;

    public ReportBasic() {
        // Required empty public constructor


    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        refActivity = ((ReportActivity)getActivity());
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_report_basic, container, false);
        next = root.findViewById(R.id.basicNext);
        titulo = root.findViewById(R.id.basicTitulo);
        descripcion = root.findViewById(R.id.basicDescripcion);


        areas2 = new ArrayList<>();
        tipos = new ArrayList<>();


        area = root.findViewById(R.id.sArea);
        tipo = root.findViewById(R.id.sTipo);

        areaAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item,areas2);
        tipoAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item,tipos);
        tipo.setAdapter(tipoAdapter);
        area.setAdapter(areaAdapter);
        area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tipos.clear();
                Log.d("TAG","asas-----"+position);

                ArrayList<String> a= (ArrayList<String>) areasTipo.get(areas2.get(position));
                if(a==null){
                    a=new ArrayList<>();
                }
                Log.d("TAG","asas-----"+a);
                tipos.add("-");
                tipos.addAll(a);
                tipoAdapter.notifyDataSetChanged();

                tipo.setSelection(indexTipoSelection);
                indexTipoSelection=0;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        if(savedInstanceState==null){
            tipos.add("-");
            areas2.add("-");
            DocumentReference docRef = refActivity.db.collection("data").document("areas_tipo");
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            areasTipo = document.getData();
                            Set<String> s = new HashSet<>();
                            s = areasTipo.keySet();
                            for (String a:s) {
                                areas2.add(a);
                            }
                            Log.d("TAG", "asasas "+ areas2);
                        } else {
                            Log.d("TAG", "No such document");
                        }
                        areaAdapter.notifyDataSetChanged();
                    } else {
                        Log.d("TAG", "get failed with ", task.getException());
                    }
                }
            });
        }
        else {
            Log.d("TAG","onViewStateRestored"+savedInstanceState.getInt("area"));
            titulo.setText(savedInstanceState.getString("titulo")+"----");
            descripcion.setText(savedInstanceState.getString("descripcion"));
            areasTipo = ((Map) savedInstanceState.getSerializable("mapAreasTipo"));
            areas2.addAll((List<String>) savedInstanceState.getSerializable("areas"));
            tipos.addAll((List<String>) savedInstanceState.getSerializable("tipos"));

            Log.d("TAG","areas:"+areas2);
            Log.d("TAG","tipos:"+tipos);
            areaAdapter.notifyDataSetChanged();
            tipoAdapter.notifyDataSetChanged();
            indexTipoSelection =  savedInstanceState.getInt("tipo");
            area.setSelection(savedInstanceState.getInt("area"));
            Log.d("TAG","INDEX TIPO:"+savedInstanceState.getInt("tipo"));

           // tipo.setSelection(indexTipoSelection);
        }

/*
        if(savedInstanceState!=null && !savedInstanceState.isEmpty() ){

        }

*/
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                if(allGood()) {
                    refActivity.report.put("area", areas2.get(area.getSelectedItemPosition()));
                    refActivity.report.put("tipo", tipos.get(tipo.getSelectedItemPosition()));
                    refActivity.report.put("titulo", titulo.getText().toString());
                    refActivity.report.put("descripcion", descripcion.getText().toString());
                    refActivity.report.put("fecha", Calendar.getInstance().getTime());

                    CViewPager viewPager = getActivity().findViewById(R.id.view_page_report);
                    viewPager.setCurrentItem(1);
                }

            }
        });
        return root;
    }


    private boolean allGood(){
        return area.getSelectedItemPosition()!=0 && tipo.getSelectedItemPosition()!=0
                && titulo.getText().toString()!="" && descripcion.getText().toString()!="";
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("titulo", titulo.getText().toString());
        outState.putString("descripcion", descripcion.getText().toString());
        outState.putInt("area", area.getSelectedItemPosition());
        outState.putInt("tipo", tipo.getSelectedItemPosition());
        outState.putSerializable("areas", (Serializable) areas2);
        outState.putSerializable("tipos", (Serializable) tipos);
        outState.putSerializable("mapAreasTipo", (Serializable) areasTipo);

    }


}
