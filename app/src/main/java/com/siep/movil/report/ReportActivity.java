package com.siep.movil.report;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.siep.movil.R;
import com.siep.movil.register.main.CViewPager;
import com.siep.movil.report.main.SectionsPagerAdapter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ReportActivity extends AppCompatActivity {
    public Map<String, Object> report;
    public FirebaseFirestore db;
    public FirebaseAuth mAuth;
    public StorageReference mStorageRef;
    public FirebaseUser user;

    @Override
    protected void onStart() {
        super.onStart();
        user = mAuth.getCurrentUser();
        if(user==null){
            Toast.makeText(getApplicationContext(),"NO LOGEADO",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState!=null) report = (Map<String, Object>) savedInstanceState.getSerializable("reporte");
        else report= new HashMap<>();

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        setContentView(R.layout.activity_report);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        CViewPager viewPager = findViewById(R.id.view_page_report);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
    }

    public Task saveReport(){
         return db.collection("reportes").add(report);
    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("reporte", (Serializable) report);
    }
}