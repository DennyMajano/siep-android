package com.siep.movil.report.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.siep.movil.R;
import com.siep.movil.historial.main.HistorialActivity;
import com.siep.movil.report.MapsActivity;
import com.siep.movil.report.ReportActivity;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportData extends Fragment {
    final int AUTOCOMPLETE_REQUEST_CODE = 1;
    final int MAP_REQUEST_CODE = 2;
    final int IMG1_REQUEST_CODE = 3;
    final int IMG2_REQUEST_CODE = 4;
    final int IMG3_REQUEST_CODE = 5;

    TextView addLocation;
    double  lat=0, longi =0;
    ImageView img1, img2, img3;
    List<String> photos;
    Map<String, Uri> images;
    List<ByteArrayInputStream> arrayImg;



    ReportActivity refActivity;
    // Set the fields to specify which types of place data to
// return after the user has made a selection.
    List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME);

    private static final String TAG = "DATOS";
    Button btn;
    private boolean error;
    private final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 12345;

    public ReportData() {
        // Required empty public constructor
        photos = new ArrayList<>();
        images = new HashMap<>();
    }
    // Initialize the AutocompleteSupportFragment.



    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_report_data, container, false);
        refActivity = ((ReportActivity)getActivity());

        if(!Places.isInitialized()){
            Places.initialize(getContext(), "AIzaSyCwwFdOS80L625KeSAVu06BjV58_6AmR9g");
        }

        btn = view.findViewById(R.id.dataSave);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(lat!=0 && images.size()>0){
                    processImages();
                    uploadImages();
                }
                else {
                    Toast.makeText(getActivity(), "Complete los campos", Toast.LENGTH_SHORT).show();
                }



            }
        });

        addLocation = view.findViewById(R.id.location);
        addLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Here, thisActivity is the current activity
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_FINE_LOCATION);
                } else {
                   openMaps();
                }


            }
        });


        img1 = view.findViewById(R.id.img1);
        img2 = view.findViewById(R.id.img2);
        img3 = view.findViewById(R.id.img3);
        img2.setEnabled(false);
        img2.setVisibility(View.INVISIBLE);
        img3.setEnabled(false);
        img3.setVisibility(View.INVISIBLE);
        if (savedInstanceState !=null){
            addLocation.setText(savedInstanceState.getString("localizacion"));
            lat = savedInstanceState.getDouble("lat");
            longi = savedInstanceState.getDouble("long");
            Log.d("TAG","RECOVERED lat:"+lat+" long"+longi);
            images =(HashMap<String, Uri>) savedInstanceState.getSerializable("images");
            Log.d("TAG", "SIZEHASH"+images.size());
            if (images.containsKey("img1")){
                img1.setImageURI(images.get("img1"));
                img1.setVisibility(View.VISIBLE);
                img1.setEnabled(true);
                img2.setVisibility(View.VISIBLE);
                img2.setEnabled(true);
                Log.d("TAG","IMAG1");
            }
            if (images.containsKey("img2")){
                img2.setImageURI(images.get("img2"));
                img2.setVisibility(View.VISIBLE);
                img2.setEnabled(true);
                img3.setVisibility(View.VISIBLE);
                img3.setEnabled(true);
                Log.d("TAG","IMAG2");
            }
            if (images.containsKey("img3")){
                img3.setImageURI(images.get("img3"));
                img3.setVisibility(View.VISIBLE);
                img3.setEnabled(true);
                Log.d("TAG","IMAG3");
            }
        }
        else {
            Log.d("TAG","NO INSTANCE");

        }


        img1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");

                Intent pickImageIntent = new Intent();
                pickImageIntent.setType("image/*");

                pickImageIntent.setAction(Intent.ACTION_GET_CONTENT);

                Intent chooserIntent = Intent.createChooser(pickIntent,"Seleccione una imagen");

                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});
                startActivityForResult(chooserIntent,IMG1_REQUEST_CODE);
            }
        });

        img2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");

                Intent pickImageIntent = new Intent();
                pickImageIntent.setType("image/*");

                pickImageIntent.setAction(Intent.ACTION_GET_CONTENT);

                Intent chooserIntent = Intent.createChooser(pickIntent,"Seleccione una imagen");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});
                startActivityForResult(chooserIntent,IMG2_REQUEST_CODE);
            }
        });

        img3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");

                Intent pickImageIntent = new Intent();
                pickImageIntent.setType("image/*");

                pickImageIntent.setAction(Intent.ACTION_GET_CONTENT);

                Intent chooserIntent = Intent.createChooser(pickIntent,"Seleccione una imagen");

                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});
                startActivityForResult(chooserIntent,IMG3_REQUEST_CODE);
            }
        });



        return view;
        // Specify the types of place data to return.

    }

    private void openMaps() {
        Intent intent = new Intent(getContext(), MapsActivity.class);
        startActivityForResult(intent,MAP_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        else if (requestCode == MAP_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                Log.i(TAG, "Lat"+data.getExtras().getDouble("latitude") );
                Log.i(TAG, "Long"+data.getExtras().getDouble("longitude") );
                lat = data.getExtras().getDouble("latitude");
                longi = data.getExtras().getDouble("longitude");

                //int indexSV = data.getExtras().getString("address").indexOf("El Salvador");

                String addressTemp = data.getExtras().getString("address");
                String adminAreaTemp = data.getExtras().getString("adminArea");

                int indexSM = adminAreaTemp.indexOf(" Department");
                if(indexSM>=0){
                    adminAreaTemp = adminAreaTemp.substring(0,indexSM);
                }


                addressTemp = addressTemp +"," +adminAreaTemp;
                refActivity.report.put("departamento",adminAreaTemp);
                addLocation.setText(addressTemp);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Log.i(TAG, "Ltsn AAAAAAAAAAAAAAAAAAAAAAAaaWWWWW" );
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        else if (requestCode == IMG1_REQUEST_CODE && data != null){
            if(resultCode == RESULT_OK ){
                img1.setImageURI(data.getData());
                images.put("img1",data.getData());
                img2.setEnabled(true);
                img2.setVisibility(View.VISIBLE);
            }

        }
        else if (requestCode == IMG2_REQUEST_CODE && data != null){
            if(resultCode == RESULT_OK ){
                img2.setImageURI(data.getData());
                images.put("img2",data.getData());
                img3.setEnabled(true);
                img3.setVisibility(View.VISIBLE);
            }

        }
        else if (requestCode == IMG3_REQUEST_CODE && data != null){
            if(resultCode == RESULT_OK ){
                img3.setImageURI(data.getData());
                images.put("img3",data.getData());
            }

        }
    }

    private void saveReport(){
        refActivity.report.put("lugar",addLocation.getText());
        Log.d("TAG","lat:"+lat+" long"+longi);
        refActivity.report.put("ubicacion",new GeoPoint(lat,longi));
        refActivity.report.put("fotografia",photos);
        refActivity.report.put("user",refActivity.db.collection("usuariosmovil").document(refActivity.user.getUid()));
        Log.d("TAG",refActivity.report+"----");
        Task result = refActivity.saveReport();
        result.addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()){
                    Toast.makeText(getContext(),"Reporte guardado",Toast.LENGTH_LONG).show();
                    Intent intent =new Intent(getActivity(), HistorialActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        });

    }




    private  void  processImages(){
        //ByteArrayInputStream img1,img2,img3=null;
        arrayImg= new ArrayList<>();
        try {

            for (byte i=0; i<images.size();i++){
               arrayImg.add( getInputStreamFormUri(getActivity(), images.get("img"+(i+1))));
            }
/*
            if (images.size()==1){
                img1 = getInputStreamFormUri(getActivity(), images.get("img1"));
                arrayImg.add(img1);
            }
            else if (images.size()==2){
                img1 = getInputStreamFormUri(getActivity(), images.get("img1"));
                img2 = getInputStreamFormUri(getActivity(), images.get("img2"));
                arrayImg.add(img1);
                arrayImg.add(img2);
            }
            else if (images.size()==3){
                img1 = getInputStreamFormUri(getActivity(), images.get("img1"));
                img2 = getInputStreamFormUri(getActivity(), images.get("img2"));
                img3 = getInputStreamFormUri(getActivity(), images.get("img3"));
                arrayImg.add(img1);
                arrayImg.add(img2);
                arrayImg.add(img3);
            }
*/


        } catch (IOException e) {
            e.printStackTrace();
        }


    }
/*
    private void uploadImages(){

        final StorageReference imageRef =  refActivity.mStorageRef.child("images/"+(+Calendar.getInstance().getTimeInMillis())+"-"+Math.random());

        imageRef.putStream(arrayImg.get(arrayImg.size()-1)).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Toast.makeText(getContext(),"imagen subida",Toast.LENGTH_SHORT).show();
                Log.d("TAG","xxxxxxxxx-"+imageRef.getDownloadUrl().toString());
                photos.add(imageRef.getDownloadUrl().toString());
                arrayImg.remove(arrayImg.size()-1);
                if(arrayImg.size()>=1){
                    uploadImages();
                }
                else {
                    saveReport();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("TAG",e.getMessage());
                Toast.makeText(getContext(),"imagen error",Toast.LENGTH_SHORT).show();
                setError();
            }
        });

    }
*/
    private void uploadImages(){
        final StorageReference imageRef =  refActivity.mStorageRef.child("images/"+(+Calendar.getInstance().getTimeInMillis())+"-"+Math.random());
        UploadTask uploadTask = imageRef.putStream(arrayImg.get(arrayImg.size()-1));

        Task<Uri> urlTask =uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return imageRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    Toast.makeText(getContext(),"imagen subida",Toast.LENGTH_SHORT).show();
                    Log.d("TAG",downloadUri.toString());
                    photos.add(downloadUri.toString());
                    arrayImg.remove(arrayImg.size()-1);
                    if(arrayImg.size()>=1){
                        uploadImages();
                    }
                    else {
                        saveReport();
                    }
                } else {
                    Toast.makeText(getContext(),"imagen error",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }







    /**
     * Get pictures through uri and compress them
     *
     * @param uri
     */
    public static ByteArrayInputStream getInputStreamFormUri(Activity ac, Uri uri) throws FileNotFoundException, IOException {
        InputStream input = ac.getContentResolver().openInputStream(uri);
        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();
        int originalWidth = onlyBoundsOptions.outWidth;
        int originalHeight = onlyBoundsOptions.outHeight;
        if ((originalWidth == -1) || (originalHeight == -1))
            return null;
        //Image resolution is based on 480x800
        float hh = 800f;//The height is set as 800f here
        float ww = 480f;//Set the width here to 480f
        //Zoom ratio. Because it is a fixed scale, only one data of height or width is used for calculation
        int be = 1;//be=1 means no scaling
        if (originalWidth > originalHeight && originalWidth > ww) {//If the width is large, scale according to the fixed size of the width
            be = (int) (originalWidth / ww);
        } else if (originalWidth < originalHeight && originalHeight > hh) {//If the height is high, scale according to the fixed size of the width
            be = (int) (originalHeight / hh);
        }
        if (be <= 0)
            be = 1;
        //Proportional compression
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = be;//Set scaling
        bitmapOptions.inDither = true;//optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        input = ac.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();

        return compressImage(bitmap);//Mass compression again
    }

    /**
     * Mass compression method
     *
     * @param image
     * @return
     */
    public static ByteArrayInputStream compressImage(Bitmap image) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);//Quality compression method, here 100 means no compression, store the compressed data in the BIOS
        int options = 100;
        while (baos.toByteArray().length / 1024 > 100) {  //Cycle to determine if the compressed image is greater than 100kb, greater than continue compression
            baos.reset();//Reset the BIOS to clear it
            //First parameter: picture format, second parameter: picture quality, 100 is the highest, 0 is the worst, third parameter: save the compressed data stream
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);//Here, the compression options are used to store the compressed data in the BIOS
            options -= 10;//10 less each time
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());//Store the compressed data in ByteArrayInputStream
       //Generate image from ByteArrayInputStream data
        return isBm;
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("localizacion",addLocation.getText().toString());

        outState.putSerializable("images", (Serializable) images);
        outState.putDouble("lat",lat);
        outState.putDouble("long",longi);



    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openMaps();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }
}
