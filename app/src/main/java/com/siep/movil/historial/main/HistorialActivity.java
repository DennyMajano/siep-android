package com.siep.movil.historial.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.LoginFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QuerySnapshot;
import com.siep.movil.R;
import com.siep.movil.login.LoginActivityR;
import com.siep.movil.report.ReportActivity;
import com.siep.movil.uix.login.LoginActivity2;

import java.util.ArrayList;
import java.util.List;

public class HistorialActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private MyAdapter myAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private FloatingActionButton fbRegisterReport;
    private Toolbar toolbar;

    private FirebaseAuth mAuth;
    private FirebaseUser user;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    List<ReportModel> listReports;

    @Override
    protected void onStart() {
        super.onStart();
        user = mAuth.getCurrentUser();

        if(user!=null){
            Toast.makeText(getApplicationContext(),"Bienvenido:"+user.getPhoneNumber(),Toast.LENGTH_SHORT);
            retrieveReport();
        }
        else{
            redirectUser();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        db.setFirestoreSettings(settings);

        mAuth = FirebaseAuth.getInstance();

        setContentView(R.layout.activity_historial);
        listReports = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerviewer);
        layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);


        myAdapter = new MyAdapter(getApplicationContext(),listReports);

        recyclerView.setAdapter(myAdapter);


        fbRegisterReport = findViewById(R.id.fab);

        fbRegisterReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HistorialActivity.this, ReportActivity.class));


            }
        });
        toolbar = findViewById(R.id.toolbarHistorial);
        toolbar.setOverflowIcon(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_action_name));
        setSupportActionBar(toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.signout:
                        mAuth.signOut();
                        Intent intentOut = new Intent(HistorialActivity.this, LoginActivityR.class);
                        intentOut.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intentOut);
                }
                return false;
            }
        });


    }

    private void retrieveReport(){
        Log.d("TAG","uid:"+user.getUid());
        listReports.clear();
        db.collection("reportes").whereEqualTo("user",db.collection("usuariosmovil").document(user.getUid())).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){

                    for (DocumentSnapshot document: task.getResult().getDocuments()){
                        List<String> fotos = (ArrayList)document.get("fotografia");
                        listReports.add(
                                new ReportModel(document.get("titulo").toString(), document.get("descripcion").toString(),fotos)
                        ) ;
                    }

                    Log.d("TAG","DATOOOOS");

                    notifyToAdapter();

                }
                else{
                    Toast.makeText(getApplicationContext(),"Error al recuperar datos",Toast.LENGTH_SHORT);
                    Log.d("TAG",task.getException().getMessage());
                }
            }

        });
    }

    private void notifyToAdapter(){
        this.myAdapter.notifyDataSetChanged();
        Toast.makeText(getApplicationContext(),"Datos recuperados",Toast.LENGTH_SHORT).show();
        for (int i=0;i<listReports.size();i++){
            Log.d("TAG", listReports.get(i).getTitulo() + " => asasasas" +i+ listReports.get(i).getDescripcion());
            Log.d("TAG",myAdapter.getItemCount()+"SIZE"+listReports.size());

        }
    }

    private void redirectUser(){
        //verification successful we will start the profile activity
        Intent intent = new Intent(getApplicationContext(), LoginActivityR.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menumap, menu);
        return true;
    }


}
