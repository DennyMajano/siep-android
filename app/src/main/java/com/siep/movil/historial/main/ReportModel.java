package com.siep.movil.historial.main;

import java.util.ArrayList;
import java.util.List;

public class ReportModel {
    private String titulo;
    private String descripcion;
    private List<String> fotos = new ArrayList<>();
    ReportModel(String titulo, String descripcion, List<String> fotos){
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.fotos =fotos;
    }


    public String getTitulo() {
        return titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public List<String> getFotos() {
        return fotos;
    }
}
