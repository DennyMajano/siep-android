package com.siep.movil.historial.main;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.firestore.DocumentSnapshot;
import com.siep.movil.R;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    List<ReportModel> reportes;
    Context context;


    public MyAdapter(Context context, List<ReportModel> listReports){
        Log.d("TAG","ADAPTERZISE"+listReports.size());
        this.reportes = listReports;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list,parent,false);
        context = parent.getContext();
        MyViewHolder viewHolder= new MyViewHolder(view);
        return  viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.titulo.setText(reportes.get(position).getTitulo());
        holder.descripcion.setText(reportes.get(position).getDescripcion());
        Glide.with(context).load(reportes.get(position).getFotos().get(0)).into(holder.imgReport);

    }

    @Override
    public int getItemCount() {
        return reportes.size();
    }

    public static class MyViewHolder extends  RecyclerView.ViewHolder{
        TextView titulo, descripcion;
        ImageView imgReport;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            titulo = itemView.findViewById(R.id.titleReport);
            descripcion = itemView.findViewById(R.id.textreport);
            imgReport = itemView.findViewById(R.id.imgReport);


        }


    }
}
